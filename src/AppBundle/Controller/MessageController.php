<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Message;
use AppBundle\Entity\Offer;
use AppBundle\Form\MessageType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;

class MessageController extends Controller
{
    /**
     * @Route("/message/add", name="message_add")
     */
    public function addMessageAction(Request $request)
    {
        $form = $this->createForm(MessageType::class, new Message(), [
            'csrf_protection' => false,
        ]);

        $offer = $this->getDoctrine()->getRepository(Offer::class)->find($request->get('offer'));

        if (!$offer) {
            return $this->error(Response::HTTP_BAD_REQUEST, 'offer not found');
        }

        $isSeller = false;
        if (empty($request->request->get('demandToken'))) {
            if ($this->getUser() !== $offer->getUser()) {
                return $this->error(Response::HTTP_UNAUTHORIZED, 'Access error');
            }
            $isSeller = true;
        } else {
            if ($offer->getDemand()->getToken() !== $request->get('demandToken')) {
                return $this->error(Response::HTTP_BAD_REQUEST, 'incorrect value');
            }
            $isSeller = false;
        }

        $request->request->remove('demandToken');
        $form->submit($request->request->all());

        if ($form->isValid()) {
            $message = $form->getData();
            $em = $this->getDoctrine()->getManager();

            $message->setSeller($isSeller);

            $em->persist($message);
            $em->flush();

            return $this->success($message, Response::HTTP_CREATED);
        }

        return $this->error(Response::HTTP_BAD_REQUEST, 'incorrect value');
    }

    protected function success($data = null, $code = Response::HTTP_OK)
    {
        $response = [
            'status' => 'success',
            'code' => $code,
            'data' => $data,
        ];

        return $this->json($response, $code);
    }

    protected function error($code, $message)
    {
        $response = [
            'status' => 'error',
            'code' => $code,
            'message' => $message,
        ];

        return $this->json($response, $code);

    }

    /**
     * @Route("/message/show_all/{offerId}", name="message_show_all")
     */
    public function showAllAction(Request $request, $offerId)
    {
        $offer = $this->getDoctrine()->getRepository(Offer::class)->find($offerId);

        $seller = true;
        if ($request->get('token') === $offer->getDemand()->getToken()){
            $seller = false;
        }

        $messages = $this->getDoctrine()->getRepository(Message::class)->findBy([
            'offer' => $offerId,
        ]);

        return $this->render('AppBundle:Message:show_all.html.twig', [
            'messages' => $messages,
            'seller' => $seller,
        ]);
    }

}
<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Demand;
use AppBundle\Entity\Offer;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class PayController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('', array('name' => $name));
    }

    /**
     * @Route("/pay/token/{token}/payOffer/{offerId}", name="pay")
     */
    public function payAction($token, $offerId)
    {
        return $this->render("@App/PayController/pay.html.twig", [
            'token' => $token,
            'offerId' => $offerId,
        ]);
    }

    /**
     * @Route("/pay/token/{token}/payOffer/{offerId}/success", name="pay_success")
     */
    public function successAction($token, $offerId)
    {
        $em = $this->getDoctrine()->getManager();
        $demand = $this->getDoctrine()->getRepository(Demand::class)->findOneBy([
            'token' => $token,
        ]);
        $offer = $this->getDoctrine()->getRepository(Offer::class)->findOneBy([
            'id' => $offerId
        ]);
        $demand->setStatus(Demand::STATUS_PAID);
        $offer->setStatus(Offer::STATUS_PAID);
        $em->flush();
        $this->addFlash(
            'success',
            'Вы успешно оплатили товар!'
        );
        return $this->redirectToRoute('show_by_token', [
            'token' => $token,
        ]);
    }

    /**
     * @Route("/pay/token/{token}/payOffer/{offerId}/error", name="pay_error")
     */
    public function errorAction($token, $offerId) {
        $this->addFlash(
            'error',
            'Не удалось оплатить товар!'
        );
        return $this->redirectToRoute('show_by_token', [
            'token' => $token,
        ]);
    }
}
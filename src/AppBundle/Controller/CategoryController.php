<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Category;
use AppBundle\Form\CategoryType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class CategoryController extends Controller
{
    /**
     * @Route("/category/create", name="category_create")
     */
    public function createAction(Request $request)
    {
        $category = new Category();
        $form = $this->createForm(CategoryType::class, $category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($category);
            $em->flush();

//            return $this->redirectToRoute('category_create');
        }

        return $this->render('AppBundle:Category:create.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/category/list")
     */
    public function listAction()
    {
        $categories = $this->getDoctrine()->getRepository(Category::class)->findAll();
        return $this->render('AppBundle:Category:list.html.twig', array(
            'categories' =>$categories,
        ));
    }

    /**
     * @Route("/category/delete/{categoryId}")
     */
    public function deleteAction($categoryId)
    {
        $em = $this->getDoctrine()->getManager();
        $category = $em->getRepository('AppBundle:Category')->find($categoryId);

        if (!$category) {
            throw $this->createNotFoundException(
                'No category found for id ' . $categoryId
            );
        }

        $em->remove($category);
        $em->flush();

        return $this->listAction();
    }

}

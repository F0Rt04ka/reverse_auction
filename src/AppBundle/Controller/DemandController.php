<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Demand;
use AppBundle\Entity\Dispute;
use AppBundle\Entity\Offer;
use AppBundle\Entity\User;
use AppBundle\Form\CommentType;
use AppBundle\Form\DemandType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;


class DemandController extends Controller
{
    /**
     * @Route("/demand/token/{token}", name="show_by_token")
     */
    public function showByTokenAction($token)
    {
        $demand = $this->getDoctrine()->getRepository(Demand::class)->findOneBy([
            'token' => $token,
        ]);
        $offers = $this->getDoctrine()->getRepository(Offer::class)->findBy([
            'demand' => $demand->getId(),
        ]);

        return $this->render('AppBundle:DemandController:show_by_token.html.twig', array(
            'demand' => $demand,
            'offers' => $offers,
        ));
    }

    /**
     * @Route("/demand/id/{id}")
     */
    public function showByIdAction($id)
    {
        $demand = $this->getDoctrine()->getRepository(Demand::class)->findOneBy([
            'id' => $id,
        ]);
        $offers = $demand->getOffers();

        $sendOffer = null;
        foreach ($offers as $offer) {
            if ($offer->getUser() === $this->getUser()) {
                $sendOffer = $offer;
            }
        }


        return $this->render('AppBundle:DemandController:show_by_id.html.twig', array(
            'demand' => $demand,
            'user'   => $this->getUser(),
            'offer'  => $sendOffer,
        ));
    }

    /**
     * @Route("/", name="demand_create")
     */
    public function createAction(Request $request)
    {
        $demand = new Demand();
        $form = $this->createForm(DemandType::class, $demand);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $demand->setStatus(Demand::STATUS_OPEN);
            $demand->setTokenValue();

            $message = \Swift_Message::newInstance()
                ->setSubject('Вы создали заявку')
                ->setFrom('reverse.auction@yandex.ru')
                ->setTo($demand->getEmail())
                ->setBody(
                    $this->renderView(
                        'AppBundle:Email:demand_created.html.twig', [
                            'token' => $demand->getToken(),
                            'domain' => $_SERVER['SERVER_NAME'],
                        ]
                    ),
                'text/html'
            );
            $this->get('mailer')->send($message);
            $em->persist($demand);
            $em->flush();
            $this->addFlash(
                'success',
                'Заявка создана успешно!'
            );

            return $this->redirectToRoute('show_by_token', [
                'token' => $demand->getToken(),
            ]);
        }

        return $this->render('AppBundle:DemandController:create.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/demand/token/{token}/edit")
     */
    public function editAction(Request $request, $token)
    {
        $demand = $this->getDoctrine()->getRepository(Demand::class)->findOneBy([
            'token' => $token,
        ]);

        $form = $this->createForm(DemandType::class, $demand);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($demand);
            $em->flush();
            $this->addFlash(
                'success',
                'Заявка отредактирована успешно!'
            );

            return $this->redirectToRoute('show_by_token', [
                'token' => $token
            ]);
        }

        return $this->render('AppBundle:DemandController:edit.html.twig', [
            'demand' => $demand,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/demands", name="demand_list")
     */
    public function listAction()
    {
        $user = $this->getUser();
        $categories = $user->getCategories();
        $demands = $this->getDoctrine()->getRepository(Demand::class)->findByCategories($categories);

        return $this->render('AppBundle:DemandController:list.html.twig', array(
            'demands' => $demands,
            'user' => $user,
        ));
    }

    /**
     * @Route("/demand/token/{token}/showOffer/{offerId}", name="demand_offer_show")
     */
    public function showOfferAction($token, $offerId)
    {
        $offer = $this->getDoctrine()->getRepository(Offer::class)->find($offerId);
        $dispute = $this->getDoctrine()->getRepository(Dispute::class)->findOneBy([
            'demand' => $offer->getDemand()->getId()
        ]);

        return $this->render('AppBundle:DemandController:show_offer.html.twig', array(
            'offer' => $offer,
            'dispute' => $dispute,
        ));
    }

    /**
     * @Route("/demand/token/{token}/selectOffer/{offerId}")
     */
    public function selectOfferAction($token, $offerId)
    {
        $em = $this->getDoctrine()->getManager();
        $demand = $this->getDoctrine()->getRepository(Demand::class)->findOneBy([
            'token' => $token,
        ]);
        $offer = $this->getDoctrine()->getRepository(Offer::class)->findOneBy([
            'id' => $offerId
        ]);
        $offer->setDemand($demand);
        $demand->setStatus(Demand::STATUS_OFFER_SELECTED);
        $offer->setStatus(Offer::STATUS_SELECTED);
        $em->flush();

        return $this->redirectToRoute('show_by_token', [
            'token' => $token,
        ]);
    }

    /**
     * @Route("/demand/token/{token}/payOffer/{offerId}")
     */
    public function payOfferAction($token, $offerId)
    {
        $em = $this->getDoctrine()->getManager();
        $demand = $this->getDoctrine()->getRepository(Demand::class)->findOneBy([
            'token' => $token,
        ]);
        $offer = $this->getDoctrine()->getRepository(Offer::class)->findOneBy([
            'id' => $offerId
        ]);
        return $this->redirectToRoute('pay', [
            'token' => $token,
            'offerId' => $offerId,
        ]);
    }

    /**
     * @Route("/demand/{token}/cancel")
     */
    public function cancelOfferAction(Request $request, $token)
    {
        $demand = $this->getDoctrine()->getRepository(Demand::class)->findOneBy([
            'token' =>$token,
        ]);
        if (!$demand) {
            throw $this->createNotFoundException('Нет такой заявки');
        }

        $offer = $this->getDoctrine()->getRepository(Offer::class)->findOneBy([
            'demand' => $demand->getId(),
            'status' => Offer::STATUS_SELECTED,
        ]);

        $form = $this->createForm(CommentType::class, $offer);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $offer->setStatus(Offer::STATUS_OPEN);
            $demand->setStatus(Demand::STATUS_OPEN);

            $em->flush();
            return $this->redirectToRoute($request->headers->get('referer'));
        }

        return $this->render('@App/Comment/create_comment.html.twig', array(
            'form' => $form->createView(),
        ));
    }
}
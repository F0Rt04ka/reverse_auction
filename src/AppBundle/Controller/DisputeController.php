<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Demand;
use AppBundle\Entity\Dispute;
use AppBundle\Entity\Offer;
use AppBundle\Form\DisputeType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class DisputeController extends Controller
{
    /**
     * @Route("demand/token/{token}/showOffer/{offerId}/creteDispute/", name="dispute_create")
     */
    public function createAction(Request $request, $token, $offerId)
    {
        $demand = $this->getDoctrine()->getRepository(Demand::class)->findOneBy([
            'token' => $token,
        ]);

        if ($demand === null) {
            throw $this->createNotFoundException('The demand does not exist');
        }

        $offer = $this->getDoctrine()->getRepository(Offer::class)->findOneBy([
            'id' => $offerId,
            'demand' => $demand->getId(),
        ]);

        if ($offer === null) {
            throw $this->createNotFoundException('The offer does not exist');
        }

        if ($dispute = $this->getDoctrine()->getRepository(Dispute::class)->findOneBy([
            'demand' => $demand->getId(),
        ])) {
            $this->redirectToRoute('dispute_show', [
                'token' => $token,
                'offerId' => $offerId,
            ]);
        }

        $dispute = new Dispute();
        $form = $this->createForm(DisputeType::class, $dispute);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $demand = $em->getRepository(Demand::class)->findOneBy([
                'token' => $token,
            ]);

            $dispute->setStatus(Dispute::STATUS_OPEN);
            $dispute->setDemand($demand);
            $dispute->setOffer($offer);

            $em->persist($dispute);
            $em->flush();

            return $this->redirectToRoute('demand_offer_show', [
                'token' => $token,
                'offerId' => $offerId,
            ]);
        }

        return $this->render('AppBundle:Dispute:create.html.twig', [
            'form' => $form->createView(),
            'offer' => $offer,
            'demand' => $offer->getDemand(),
        ]);
    }

    /**
     * @Route("demand/token/{token}/showOffer/{offerId}/showDispute/", name="dispute_show")
     */
    public function showAction($token, $offerId)
    {
        $demand = $this->getDoctrine()->getRepository(Demand::class)->findOneBy([
            'token' => $token,
        ]);

        if ($demand === null) {
            throw $this->createNotFoundException('The demand does not exist');
        }

        $offer = $this->getDoctrine()->getRepository(Offer::class)->findOneBy([
            'id' => $offerId,
            'demand' => $demand->getId(),
        ]);

        if ($offer === null) {
            throw $this->createNotFoundException('The offer does not exist');
        }

        $dispute = $this->getDoctrine()->getRepository(Dispute::class)->findOneBy([
            'demand' => $demand->getId(),
        ]);

        return $this->render('AppBundle:Dispute:show.html.twig', [
            'dispute' => $dispute,
            'demand' => $demand,
            'offer' => $offer,
        ]);
    }

    /**
     * @Route("/dispute/close/{token}")
     */
    public function closeAction($token)
    {
        $em = $this->getDoctrine()->getManager();

        $demand = $this->getDoctrine()->getRepository(Demand::class)->findOneBy([
            'token' => $token,
        ]);

        $dispute = $this->getDoctrine()->getRepository(Dispute::class)->findOneBy([
            'demand' => $demand->getId(),
        ]);
        $dispute->setStatus(Dispute::STATUS_CLOSED);
        $em->flush();

        return $this->redirectToRoute("dispute_show", [
            'token' => $token,
        ]);
    }
}
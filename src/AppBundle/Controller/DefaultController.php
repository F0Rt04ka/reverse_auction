<?php

namespace AppBundle\Controller;

use AppBundle\Form\DemandType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Form\CategoryType;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    /**
 * @Route("/asd", name="homepage")
 */
    public function indexAction(Request $request)
    {

//        $demandForm = $this->createForm(DemandType::class);
        return $this->redirectToRoute('demand_create');
        // replace this example code with whatever you need
//        return $this->render('AppBundle:DemandController:create.html.twig', [
//            'form' => $demandForm->createView(),
//        ]);
    }
}

<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Comment;
use AppBundle\Entity\Demand;
use AppBundle\Entity\Offer;
use AppBundle\Form\CommentType;
use AppBundle\Form\OfferType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class OfferController extends Controller
{
    /**
     * @Route("/demand/id/{demandId}/", name="offer_create")
     */
    public function createAction(Request $request, $demandId)
    {
        $demand = $this->getDoctrine()->getRepository(Demand::class)->find($demandId);
        if ($demand === null) {
            throw $this->createNotFoundException('The demand does not exist');
        }

        $offer = new Offer();
        $form = $this->createForm(OfferType::class, $offer);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $demand = $em->getRepository(Demand::class)->find($demandId);

            $offer->setStatus(Offer::STATUS_OPEN);
            $offer->setDemand($demand);
            $offer->setUser($this->getUser());
            $this->getUser()->addOffer($offer);

            $message = \Swift_Message::newInstance()
                ->setSubject('Вам поступило новое предложение')
                ->setFrom('reverse.auction@yandex.ru')
                ->setTo($demand->getEmail())
                ->setBody(
                    $this->renderView(
                        'AppBundle:Email:offer_added.html.twig',
                        [
                            'token' => $demand->getToken(),
                            'offerId' => $offer->getId(),
                            'domain' => $_SERVER['SERVER_NAME'],
                        ]
                    ),
                    'text/html'
                );
            $this->get('mailer')->send($message);

            $em->persist($offer);
            $em->flush();

            $this->addFlash(
                'success',
                'Ваше предложение создано'
            );

            return $this->redirectToRoute('demand_list');
        }

        return $this->render('AppBundle:Offer:create.html.twig', array(
            'form'   => $form->createView(),
            'demand' => $demand,
        ));
    }

    /**
     * @Route("/listByDemand/{demandId}")
     */
    public function listByDemandAction($demandId)
    {
        $offers = $this->getDoctrine()->getRepository(Offer::class)->findBy([
            'demand' => $demandId,
        ]);

        return $this->render('AppBundle:Offer:list_by_demand.html.twig', array(
            'offers' => $offers,
        ));
    }

    /**
     * @Route("/offer/{offerId}/edit")
     */
    public function editAction(Request $request, $offerId)
    {
        $offer = $this->getDoctrine()->getRepository(Offer::class)->findOneBy([
            'id' =>$offerId,
        ]);
        $form = $this->createForm(OfferType::class, $offer);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($offer);
            $em->flush();
            $this->addFlash(
                'success',
                'Ваше предложение успешно отредактировано!'
            );

            return $this->redirectToRoute('show_offer', [
                'offerId' => $offerId,
            ]);
        }

        return $this->render('@App/Offer/edit.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/offer/{offerId}/show", name="show_offer")
     */
    public function showAction($offerId)
    {
        $offer = $this->getDoctrine()->getRepository(Offer::class)->findOneBy([
            'id' =>$offerId,
            'user' => $this->getUser()->getId(),
        ]);

        return $this->render('AppBundle:Offer:show.html.twig', array(
            'offer' => $offer,
            'demand' => $offer->getDemand(),
        ));
    }

    /**
     * @Route("/offer/cancel")
     */
    public function cancelOfferAction(Request $request)
    {
        $offer = $this->getDoctrine()->getRepository(Offer::class)->findOneBy([
            'id' => $request->request->get('offerId'),
            'user' => $this->getUser(),
        ]);
        $form = $this->createForm(CommentType::class, new Comment(), [
            'csrf_protection' => false,
            'allow_extra_fields' => true,
        ]);

        $form->submit($request->request->all());

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $demand = $offer->getDemand();

            $demand->setStatus(Demand::STATUS_OPEN);
            $offer->setStatus(Offer::STATUS_CANCELED);
            $comment = $form->getData();

            $em->persist($comment);
            $em->persist($demand);
            $em->persist($offer);
            $em->flush();

            return $this->json([
                'status' => 'success',
                'code' => Response::HTTP_OK,
                'message' => 'Предложение удалено',
            ]);
        }

        return $this->json([
            'status' => 'error',
            'code' => Response::HTTP_BAD_REQUEST,
            'message' => 'Ошибка',
        ], Response::HTTP_BAD_REQUEST);
    }
}

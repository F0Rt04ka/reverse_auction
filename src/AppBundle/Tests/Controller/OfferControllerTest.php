<?php

namespace AppBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class OfferControllerTest extends WebTestCase
{
    public function testCreate()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/offer/create');
    }

    public function testListbydemand()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/listByDemand');
    }

    public function testUpdatestatus()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/updateStatus');
    }

    public function testEdit()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/offer/edit');
    }

    public function testShow()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/offer/show');
    }

}

<?php

namespace AppBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DemandControllerControllerTest extends WebTestCase
{
    public function testShowbytoken()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/demand/token/{token}');
    }

    public function testShowbyid()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/demand/id/{id}');
    }

    public function testCreate()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/demand/create');
    }

    public function testEdit()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/demand/token/{token}/edit');
    }

    public function testList()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/demands');
    }

    public function testUpdatestatus()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/demand/token/{token}/updateStatus');
    }

    public function testSelectoffer()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/demand/token/{token}/selectOffer/{offerId}');
    }

}

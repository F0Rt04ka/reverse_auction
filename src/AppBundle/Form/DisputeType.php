<?php

namespace AppBundle\Form;

use AppBundle\Entity\Dispute;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Choice;
use Vich\UploaderBundle\Form\Type\VichImageType;

class DisputeType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('reason', ChoiceType::class, [
                'choices' => [
                    'Товар не соответсвует описанию' => Dispute::REASON_DOESNT_MATCH_DESCRIPTION,
                    'Неполный комплект' => Dispute::REASON_NOT_FULL_SET,
                    'Не пришел товар' => Dispute::REASON_NOT_ARRIVED,
                    'Товар неисправен' => Dispute::REASON_DEFECTIVE,
                    'Другое' => Dispute::REASON_OTHER,

                ]
            ])
            ->add('description')
            ->add('imageFile', VichImageType::class)
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Dispute'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_dispute';
    }


}

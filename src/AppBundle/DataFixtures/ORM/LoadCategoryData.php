<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Category;

class LoadCategoryData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $electronicsCategory = new Category();
        $electronicsCategory->setName("Техника");
        $manager->persist($electronicsCategory);

        $propertyCategory = new Category();
        $propertyCategory->setName("Недвижимость");
        $manager->persist($propertyCategory);

        $transportCategory = new Category();
        $transportCategory->setName("Транспорт");
        $manager->persist($transportCategory);

        $manager->flush();

        $this->addReference('Electronics', $electronicsCategory);
        $this->addReference('Property', $propertyCategory);
        $this->addReference('Transport', $transportCategory);
    }

    public function getOrder()
    {
        return 1;
    }
}
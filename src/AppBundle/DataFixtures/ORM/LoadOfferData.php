<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Offer;

class LoadOfferData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {

            $spaceshipOffer = new Offer();
            $spaceshipOffer->setName("Космический корабль")
                ->setImageName('spaceship.jpeg')
                ->setDescription('Здравствуйте! Мы готовы предложить Вам прототип нашего нового космического корабля
                                  "Ракета-2000" по очень привлекательной цене'
                )
                ->setUser($this->getReference('space user'))
                ->setDemand($this->getReference('spaceship demand'))
                ->setPrice(150000)
                ->setStatus(Offer::STATUS_OPEN);


            $manager->persist($spaceshipOffer);
            $manager->flush();

            $this->addReference("spaceship offer", $spaceshipOffer);

    }

    public function getOrder()
    {
        return 4;
    }
}
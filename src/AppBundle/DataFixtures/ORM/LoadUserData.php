<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\User;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadUserData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function load(ObjectManager $manager)
    {
        $dnsUser = new User();
        $spaceUser = new User();

        $passwordEncoder = $this->container->get('security.password_encoder');
        $dnsEncodedPassword = $passwordEncoder->encodePassword($dnsUser, '1234');
        $spaceEncodedPassword = $passwordEncoder->encodePassword($spaceUser, '1234');

        $dnsUser
            ->setEnabled(true)
            ->setEmail('dns@mail.ru')
            ->setPassword($dnsEncodedPassword)
            ->setName('DNS')
            ->setDescription('Магазин электроники DNS')
            ->addCategory($this->getReference('Electronics'));

        $spaceUser
            ->setEnabled(true)
            ->setEmail('roskosmos@mail.ru')
            ->setPassword($spaceEncodedPassword)
            ->setName('РосКосмос')
            ->setDescription('Государственная корпорация по космической деятельности')
            ->setSite('www.roscosmos.ru')
            ->setImageName('roskosmos.jpg')
            ->addCategory($this->getReference('Property'))
            ->addCategory($this->getReference('Transport'));

        $manager->persist($dnsUser);
        $manager->persist($spaceUser);
        $manager->flush();

        $this->addReference("dns user", $dnsUser);
        $this->addReference("space user", $spaceUser);
    }

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function getOrder()
    {
        return 2;
    }
}
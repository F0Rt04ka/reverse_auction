<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Demand;
use    Symfony\Component\HttpFoundation\File\UploadedFile;

class LoadDemandData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $tvDemand = new Demand();
        $tvDemand
            ->setImageName("TV.jpeg")
            ->setName("Хочу телевизор")
            ->setDescription('Хочу качественный телевизор с большой диагональю 
                              и за небольшую цену. Желательно, чтобы по дизайну был похож на тот, который на фото'
            )
            ->setEmail('iwanttv@mail.ru')
            ->setCategory($this->getReference('Electronics'))
            ->setPrice(15000)
            ->setStatus(Demand::STATUS_OPEN)
            ->setTokenValue();

        $manager->persist($tvDemand);

        $moonDemand = new Demand();
        $moonDemand
            ->setImageName("moon.jpeg")
            ->setName("Участок на Луне")
            ->setDescription('Недавно увидел по новостям на РЕН-ТВ, что стали продавать участки на Луне.
                             Так вот, хотелось бы приобрести участок с видом на Землю, размером пол-гектара.
                             Место рядом с кратером не предлагать!'
            )
            ->setEmail('iwantmoon@mail.ru')
            ->setCategory($this->getReference('Property'))
            ->setPrice(1000)
            ->setStatus(Demand::STATUS_OPEN)
            ->setTokenValue();

        $manager->persist($moonDemand);

        $spaceshipDemand = new Demand();
        $spaceshipDemand
            ->setImageName("spaceship.jpg")
            ->setName("Космолёт")
            ->setDescription('Хочу приобрести быстрый космический корабль, как на картинке, в рабочем состоянии.'
            )
            ->setEmail('iwantspaceship@mail.ru')
            ->setCategory($this->getReference('Transport'))
            ->setPrice(100000)
            ->setStatus(Demand::STATUS_OPEN)
            ->setTokenValue();

        $manager->persist($spaceshipDemand);


        $manager->flush();

        $this->addReference("tv demand", $tvDemand);
        $this->addReference("moon demand", $moonDemand);
        $this->addReference("spaceship demand", $spaceshipDemand);

    }

    public function getOrder()
    {
        return 3;
    }
}